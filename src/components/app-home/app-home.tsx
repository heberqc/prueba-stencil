import { Component, Prop } from '@stencil/core';

@Component({
  tag: 'app-home',
  styleUrl: 'app-home.css'
})
export class AppHome {
  @Prop({ mutable: true }) nombre: string = 'Heber'
  @Prop({ mutable: true }) apellido: string = 'Quequejana'

  render() {
    return [
      <ion-header>
        <ion-toolbar color="primary">
          <ion-title>Home</ion-title>
        </ion-toolbar>
      </ion-header>,
      <ion-content padding>
        <p>
          Welcome to the PWA Toolkit. You can use this starter to build entire
          apps with web components using Stencil and ionic/core! Check out the
          README for everything that comes in this starter out of the box and
          check out our docs on <a href="https://stenciljs.com">stenciljs.com</a> to get started.
        </p>
        <input
          type="text"
          name="nombre"
          id="nombre"
          onChange={(event: any) => { this.nombre = event.target.value }}
          onKeyUp={(event: any) => { console.log(event.target.value) }}
          value={this.nombre}
        />
        <input
          type="text"
          name="apellido"
          id="apellido"
          onChange={(event: any) => { this.apellido = event.target.value }}
          onKeyUp={(event: any) => { console.log(event.target.value) }}
          value={this.apellido}
        />
        <datos-component first={this.nombre} last={this.apellido}></datos-component>
        <ion-button href={`/profile/${this.nombre}/${this.apellido}`} expand="block">Profile page</ion-button>
      </ion-content>
    ];
  }
}
