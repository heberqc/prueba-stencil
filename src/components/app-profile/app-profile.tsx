import { Component, Prop, State } from '@stencil/core';
import { sayHello } from '../../helpers/utils';

@Component({
  tag: 'app-profile',
  styleUrl: 'app-profile.css'
})
export class AppProfile {

  @State() state = false;
  @Prop({ mutable: true }) name: string;
  @Prop({ mutable: true }) lastname: string;

  formattedName(name: string): string {
    if (name && name.length > 0) {
      return name.substr(0, 1).toUpperCase() + name.substr(1).toLowerCase();
    }
    return '';
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar color="primary">
          <ion-buttons slot="start">
            <ion-back-button defaultHref="/" />
          </ion-buttons>
          <ion-title>Profile: {this.name} {this.lastname}</ion-title>
        </ion-toolbar>
      </ion-header>,
      <ion-content padding>
        <p>
          {sayHello()}! My name is {this.formattedName(this.name)} {this.formattedName(this.lastname)}. My name was passed in through a
          route param!
        </p>
        <ion-item>
          <ion-input
            required
            type="text"
            placeholder="Nombre"
            value={this.name}
            onKeyUp={(event: any) => { this.name = event.target.value }}
          />
        </ion-item>
        <ion-item>
          <ion-input
            required
            type="text"
            placeholder="Apellido"
            value={this.lastname}
            onKeyUp={(event: any) => { this.lastname = event.target.value }}
          />
        </ion-item>
        {/* <ion-item>
          <ion-label>Setting ({this.state.toString()})</ion-label>
          <ion-toggle
            checked={this.state}
            onIonChange={ev => (this.state = ev.detail.checked)}
          />
        </ion-item> */}
      </ion-content>
    ];
  }
}
