import { Component, Prop, Event, EventEmitter } from '@stencil/core';

@Component({
  tag: 'datos-component',
  styleUrl: 'datos-component.css'
})
export class DatosComponent {
  @Event() todoCompleted: EventEmitter;
  @Prop() first: string;
  @Prop() last: string;

  todoCompletedHandler(todo: any) {
    this.todoCompleted.emit(todo);
  }

  render() {
    return (
      <div>
        Hello, my name is {this.first} {this.last}
      </div>
    );
  }
}